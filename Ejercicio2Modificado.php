<html>
	<head>
		<title> Uso de Método POST </title>
	</head>
	<body>
		<h2> Conversión Décimal a Binario </h2>
		
		<form name = "frmPost" method = "POST">
			Escriba el Décimal:
			<input type = "text" name = "txtNumero" required = "required">
			<input type = "submit" name = "btnEnviar">
		</form>

		<?php 

			if (!empty($_POST['txtNumero'])){
				$numDecimal = $_POST['txtNumero'];
				echo "El numero decimal es: ". $numDecimal. "<br/>";
				echo "<table border=1>";
				echo "<tr><td>Dividiendo</td><td>Divisor</td><td>Cociente</td><td>Residuo</td></tr>";
				$numBinario = ' ';
				do{
					$numBinario = $numDecimal % 2 . $numBinario;
					
					echo "<tr><td>$numDecimal</td><td>2</td><td>".(int)($numDecimal / 2)."</td><td>".($numDecimal % 2)."</td></tr>";
					$numDecimal = (int) ($numDecimal/2);
					
				}while ($numDecimal > 0);
				echo "</table>";
					echo "Numero Binario: " . $numBinario;
				
			}
		?>
	</body>
</html>	